twill (0.9-5) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control:
    + Set Vcs-* to salsa.debian.org.
    + Remove ancient X-Python-Version field.
  * d/rules: Remove trailing whitespaces.
  * Convert git repository from git-dpm to gbp layout.
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Arnaud Fontaine ]
  * d/{watch,copyright,control}: Update upstream URL to pypi as twill.idyll.org
    is not available.
  * d/copyright: Include BSD license text. This fixes lintian warning:
    copyright-refers-to-deprecated-bsd-license-file.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:08:59 -0500

twill (0.9-4) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Arnaud Fontaine ]
  * d/control:
    + Bump Standards-Version to 3.9.8. No change needed.
    + debhelper compatibility version 5 is not recommended anymore.
    + Add dh-python to Build-Depends (dh_python2 warning).
  * d/rules: CDBS DEB_PYTHON_MODULE_PACKAGE has been removed. Closes: #828119.

 -- Arnaud Fontaine <arnau@debian.org>  Wed, 27 Jul 2016 16:13:45 +0900

twill (0.9-3) unstable; urgency=low

  * Add debian/patches/04_fix_deprecated_ClientForm.patch as ClientForm
    is now shipped with mechanize.
    + debian/rules:
      - Drop Depends on python-clientform.
    + debian/control:
      - Bump python-mechanize Depends to >= 0.2.0~.
  * debian/patches/02_remove_ext.patch:
    + Fix tests as well.
  * Add debian/patches/05_fix_missing_imports.patch.
  * debian/control:
    + Bump Standards-Version to 3.9.2. No changes needed.
  * Switch from now deprecated pysupport to dh_python2.
    + Drop debian/pyversions and debian/pycompat.
    + debian/rules:
      - Remove DEB_PYTHON_SYSTEM.
    + debian/control:
      - Add X-Python-Version field.
      - Bump python and cdbs versions.

 -- Arnaud Fontaine <arnau@debian.org>  Sun, 04 Dec 2011 23:35:55 +0900

twill (0.9-2) unstable; urgency=low

  * debian/control:
    + Replace python-dev by python in Build-Depends.
    + Bump Standards-Version to 3.8.4. No changes needed.
    + Drop now useless debian/README.source.
  * debian/copyright:
    + Update copyright years.
  * Switch to dpkg-source 3.0 (quilt) format.
    + Remove Build-Depends on quilt in debian/control.
    + Remove include of patchsys-quilt.mk in debian/rules.

 -- Arnaud Fontaine <arnau@debian.org>  Sun, 16 May 2010 13:52:13 +0100

twill (0.9-1) unstable; urgency=low

  [ Arnaud Fontaine ]
  * New upstream bugfix release.
  * debian/control:
    + Add ${misc:Depends} to Depends in case the result of a call to
      debhelper tools adds extra dependencies.
    + Drop useless Build-Depends on patchutils.
    + Bump Standards-Version to 3.8.0.
      - Add debian/README.source.
  * debian/doc-base:
    + Change the section to Programming.
  * debian/copyright:
    + Update copyright years.
    + Reorganize to make copyright information clearer.

  [ Sandro Tosi ]
  * debian/control:
    - Uniforming Vcs-Browser field.
    - Switch Vcs-Browser field to viewsvn.

 -- Arnaud Fontaine <arnau@debian.org>  Wed, 21 Jan 2009 23:54:29 +0000

twill (0.9~b1-2) unstable; urgency=low

  [ Arnaud Fontaine ]
  * New email address.
  * Use quilt patch system instead of simple patchsys.
  * Remove useless debian/control.in.
  * Drop useless dependency on python-setuptools, use distutils instead.
    + Add debian/patches/03_no_setuptools.patch.
  * debian/patches/01_fix_history_path.patch:
    + Check whether HOME environment variable is set before using it.
  * debian/control:
    + Now depend on python-mechanize >= 0.1.7b-2 because it fixes
      #456944.
    + Update Standards-Version to 3.7.3. No changes needed.

  [ Piotr Ożarowski ]
  * Added debian/watch file.
  * Vcs-Svn, Vcs-Browser and Homepage fields added (dpkg support them now).

 -- Arnaud Fontaine <arnau@debian.org>  Fri, 21 Dec 2007 03:04:41 +0000

twill (0.9~b1-1) unstable; urgency=low

  * Initial release. Closes: #387152.

 -- Arnaud Fontaine <arnaud@andesi.org>  Sun, 08 Apr 2007 10:37:43 +0200
